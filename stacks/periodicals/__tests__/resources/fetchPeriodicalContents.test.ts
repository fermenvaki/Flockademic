jest.mock('../../src/services/fetchPeriodicalContents', () => ({
  fetchPeriodicalContents: jest.fn().mockReturnValue(Promise.resolve({
    hasPart: [],
    identifier: 'arbitrary_identifier',
  })),
}));

import { fetchPeriodicalContents } from '../../src/resources/fetchPeriodicalContents';

const mockContext = {
  database: {} as any,

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/arbitrary_id/articles', 'arbitrary_id' ],
  path: '/arbitrary_id/articles',
  query: null,
};

it('should error when no journal ID was specified', () => {
  const promise = fetchPeriodicalContents({ ...mockContext, params: [], path: '/' });

  return expect(promise).rejects.toEqual(new Error('Could not find journal articles without a journal ID.'));
});

it('should error when the requested articles could not be found', () => {
  const mockedFetchArticleService = require.requireMock('../../src/services/fetchPeriodicalContents');
  mockedFetchArticleService.fetchPeriodicalContents.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = fetchPeriodicalContents(mockContext);

  return expect(promise).rejects.toEqual(new Error('Could not find articles for the journal with ID `arbitrary_id`.'));
});

it('should call the command handler when all parameters are correct', (done) => {
  const mockedFetchArticleService = require.requireMock('../../src/services/fetchPeriodicalContents');

  const promise = fetchPeriodicalContents({
    ...mockContext,
    params: [ '/some_id/articles', 'some_id' ],
    path: '/some_id/articles',
  });

  setImmediate(() => {
    expect(mockedFetchArticleService.fetchPeriodicalContents.mock.calls.length).toBe(1);
    expect(mockedFetchArticleService.fetchPeriodicalContents.mock.calls[0][1]).toBe('some_id');
    done();
  });
});
