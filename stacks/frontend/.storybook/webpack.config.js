var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function(storybookBaseConfig, env) {
  var ownConfig = require('../webpack.common.config')(env);

  storybookBaseConfig.module.rules = storybookBaseConfig.module.rules.concat(ownConfig.module.rules);
  // TSlint fails for some reason and is not necessary for rendering stories, so remove it:
  storybookBaseConfig.module.rules = storybookBaseConfig.module.rules.filter((rule) => rule.loader !== 'tslint-loader');

  storybookBaseConfig.resolve = {
    extensions: ['.js', '.css', '.ts', '.tsx'],
    modules: [path.resolve(__dirname, '../src'), 'node_modules']
  };

  storybookBaseConfig.plugins.push(
    new ExtractTextPlugin('styles.css')
  );

  return storybookBaseConfig;
};
