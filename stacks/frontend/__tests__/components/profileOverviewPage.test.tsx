jest.mock('react-ga');
jest.mock('../../src/services/periodical', () => ({
  getScholarlyArticlesForAccount: jest.fn().mockReturnValue(Promise.resolve([
    { identifier: 'arbitrary-id-1' },
    { identifier: 'arbitrary-id-2' },
  ])),
}));
jest.mock('../../src/services/account', () => ({
  getAccountId: jest.fn().mockReturnValue(Promise.resolve('arbitrary_account_id')),
  getProfiles: jest.fn().mockReturnValue(Promise.resolve([
    { identifier: 'arbitrary_account_id', name: 'Arbitrary name', sameAs: 'https://orcid.org/arbitrary_orcid' },
  ])),
}));
jest.mock('../../src/services/orcid', () => ({
  getProfile: jest.fn().mockReturnValue(Promise.resolve(
    [
      {
        description: 'Arbitrary ORCID biography',
        name: 'Arbitrary name',
        sameAs: [ { roleName: 'Arbitrary website name', sameAs: 'arbitrary-url' } ],
      },
      [],
    ]
 )),
}));

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ErrorPage } from '../../src/components/errorPage/component';
import { ProfileOverview } from '../../src/components/profileOverview/component';
import {
  BareProfileOverviewPage as ProfileOverviewPage, Props,
} from '../../src/components/profileOverviewPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

function initialisePage({
  // tslint:disable-next-line:no-unnecessary-initializer
  currentUser = undefined,
  orcid = 'arbitrary_orcid',
} = {}) {
  return shallow(
    <ProfileOverviewPage orcid={currentUser} match={{ params: { orcid }, url: 'https://arbitrary_url' }}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the profile details are still loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display the profile overview once the profile details are loaded', (done) => {
  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(toJson(page)).toMatchSnapshot();

    done();
  });
});

it('should display an error when the profile could not be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.resolve([]));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ErrorPage)).toExist();

    done();
  });
});

it('should display an error when no ORCID was given and the user is not logged in', (done) => {
  const page = initialisePage({
    currentUser: null,
    orcid: null,
  });

  setImmediate(() => {
    page.update();
    expect(page.find(ErrorPage)).toExist();

    done();
  });
});

it('should not fetch account data when no ORCID was given and the user is not logged in', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  const mockOrcidService = require.requireMock('../../src/services/orcid');

  const page = initialisePage({
    currentUser: null,
    orcid: null,
  });

  setImmediate(() => {
    page.update();
    expect(mockAccountService.getProfiles.mock.calls.length).toBe(0);
    expect(mockOrcidService.getProfile.mock.calls.length).toBe(0);

    done();
  });
});

it('should start fetching account data when the current user\'s ORCID becomes known', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  const mockOrcidService = require.requireMock('../../src/services/orcid');

  const page = initialisePage({
    currentUser: null,
    orcid: null,
  });

  setImmediate(() => {
    page.update();
    expect(mockAccountService.getProfiles.mock.calls.length).toBe(0);
    expect(mockOrcidService.getProfile.mock.calls.length).toBe(0);

    page.setProps({ orcid: 'arbitrary-orcid', verifyingOrcid: false });

    setImmediate(() => {
      page.update();

      expect(mockOrcidService.getProfile.mock.calls.length).toBe(1);
      expect(mockOrcidService.getProfile.mock.calls[0][0]).toBe('arbitrary-orcid');

      done();
    });
  });
});

it('should not start fetching account data when props change but still no ORCID is known', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  const mockOrcidService = require.requireMock('../../src/services/orcid');

  const page = initialisePage({
    currentUser: null,
    orcid: null,
  });

  setImmediate(() => {
    page.update();
    expect(mockAccountService.getProfiles.mock.calls.length).toBe(0);
    expect(mockOrcidService.getProfile.mock.calls.length).toBe(0);

    page.setProps({ arbitraryProp: 'arbitraryValue' });

    setImmediate(() => {
      page.update();

      expect(mockOrcidService.getProfile.mock.calls.length).toBe(0);

      done();
    });
  });
});

it('should redirect to the current user\'s profile page when the current user gets known', (done) => {
  const page = initialisePage({
    currentUser: null,
    orcid: null,
  });

  setImmediate(() => {
    page.update();

    page.setProps({ orcid: 'arbitrary-orcid', verifyingOrcid: false });

    setImmediate(() => {
      page.update();

      expect(page.find('Redirect[to="/profile/arbitrary-orcid"]')).toExist();

      done();
    });
  });
});

// tslint:disable-next-line:max-line-length
it('should display the spinner when the account ID could not be loaded but ORCID details could still be fetched', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getAccountId.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(new Promise(jest.fn()));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(Spinner)).toExist();

    done();
  });
});

// tslint:disable-next-line:max-line-length
it('should display the spinner when the profile could not be loaded but ORCID details could still be fetched', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(new Promise(jest.fn()));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(Spinner)).toExist();

    done();
  });
});

it('should display the profile overview even when the articles could not be loaded', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview)).toExist();

    done();
  });
});

it('should display the details known by ORCID if the local profile could not be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Some name' },
    [],
  ]));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('person').name).toBe('Some name');

    done();
  });
});

it('should pass null to the overview for articles if internal articles could not be loaded', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('articles')).toBeNull();

    done();
  });
});

it('should pass only internal articles to the overview if ORCID articles could not be loaded', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(Promise.resolve([
    { identifier: 'some-id' },
  ]));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('articles')).toEqual([ { identifier: 'some-id' } ]);

    done();
  });
});

it('should not pass articles to the overview when internal articles have not loaded yet', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(new Promise(jest.fn()));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('articles')).toBeUndefined();

    done();
  });
});

it('should send the number of external articles to analytics', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Arbitrary name' },
    [ { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' } ],
  ]));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Load ORCID profile',
      category: 'View Profile',
      label: 'ORCID works',
      value: 1,
    });

    done();
  });
});

it('should pass external articles to the overview after internal ones', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(Promise.resolve(
    [ { identifier: 'some-internal-article-id' } ],
  ));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Arbitrary name' },
    [ { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' } ],
  ]));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('articles')).toEqual([
      { identifier: 'some-internal-article-id' },
      { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
    ]);

    done();
  });
});

it('should filter out external articles that have already been linked to internal ones', (done) => {
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getScholarlyArticlesForAccount.mockReturnValueOnce(Promise.resolve(
    [
      { identifier: 'some-internal-article-id', sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
      { identifier: 'some-other-internal-article-id' },
    ],
  ));
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.getProfile.mockReturnValueOnce(Promise.resolve([
    { name: 'Arbitrary name' },
    [
      { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
      { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/42' },
    ],
  ]));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find(ProfileOverview).prop('articles')).toEqual([
      { identifier: 'some-internal-article-id', sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
      { identifier: 'some-other-internal-article-id' },
      { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/42' },
    ]);

    done();
  });
});
